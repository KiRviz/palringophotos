//
//  UIImageView+Extensions.swift
//  PalringoPhotos
//
//  Created by Darius Jankauskas on 10/08/2020.
//  Copyright © 2020 Palringo. All rights reserved.
//

import UIKit

extension UIImageView {
    func load(photo: Photo?, withTransitionView transitionView: UIView) -> URLSessionTask? {
        guard let photo = photo else { return nil }
        
        return CachedRequest.request(url: photo.url) { data, isCached in
            guard let data = data else { return }
            let img = UIImage(data: data)
            
            DispatchQueue.main.async { [weak self] in
                if isCached {
                    self?.image = img
                } else {
                    UIView.transition(with: transitionView, duration: 1, options: [.transitionCrossDissolve, .allowUserInteraction], animations: {
                        self?.image = img
                    }, completion: nil)
                }
            }
        }
    }
}
