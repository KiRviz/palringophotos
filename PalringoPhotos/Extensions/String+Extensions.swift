//
//  String+Extensions.swift
//  PalringoPhotos
//
//  Created by Darius Jankauskas on 11/08/2020.
//  Copyright © 2020 Palringo. All rights reserved.
//

import UIKit

extension String {
    
    /// Full HTML as attributted string
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    
    /// Simple String by stripping all tags
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
