//
//  UIImage+Extensions.swift
//  PalringoPhotos
//
//  Created by Darius Jankauskas on 11/08/2020.
//  Copyright © 2020 Palringo. All rights reserved.
//

import UIKit

extension UIImage {
    /// Calculates the new height of an image if its width was scaled down to
    /// `finalWidth` keeping the aspect ratio intact
    func heightAfterShrinkingTo(width finalWidth: CGFloat) -> CGFloat {
        let fittedHeight = size.height / size.width * finalWidth
        let finalHeight = size.width > finalWidth ? fittedHeight : size.height
        return finalHeight
    }
}
