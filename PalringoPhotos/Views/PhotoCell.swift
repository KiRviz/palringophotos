//
//  PhotoCell.swift
//  PalringoPhotos
//
//  Created by Benjamin Briggs on 14/10/2016.
//  Copyright © 2016 Palringo. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    
    private var fetchTask: URLSessionTask? {
        willSet {
            fetchTask?.cancel()
        }
    }
    
    public var photo: Photo? {
        didSet {
            nameLabel?.text = photo?.name ?? ""
            
            self.fetchTask = imageView?.load(photo: photo, withTransitionView: self)
        }
    }
    
    public var image: UIImage? {
        return imageView?.image
    }
    
    @IBOutlet private weak var imageView: UIImageView?
    @IBOutlet private weak var nameLabel: UILabel?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.nameLabel?.text = nil
        self.imageView?.image = nil
    }
}
