//
//  DetailsPhotoCell.swift
//  PalringoPhotos
//
//  Created by Darius Jankauskas on 11/08/2020.
//  Copyright © 2020 Palringo. All rights reserved.
//

import UIKit

class DetailsPhotoCell: UICollectionViewCell {
    
    private var fetchTask: URLSessionTask? {
        willSet {
            fetchTask?.cancel()
        }
    }
    
    public var photo: Photo? {
        didSet {
            guard let photo = photo else { return }
            
            fetchTask = CachedRequest.request(url: photo.url) { data, isCached in
                guard let data = data else { return }
                let image = UIImage(data: data)
                
                DispatchQueue.main.async { [weak self] in
                    self?.imageView?.image = image
                    
                    guard let image = image else { return }
                    
                    self?.widthConstraint.constant = UIScreen.main.bounds.width
                    self?.heightConstraint.constant = image.heightAfterShrinkingTo(width: UIScreen.main.bounds.width)
                }
            }
        }
    }
    
    @IBOutlet private weak var imageView: UIImageView?
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView?.image = nil
    }
}
