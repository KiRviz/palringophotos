//
//  LoadingView.swift
//  PalringoPhotos
//
//  Created by Darius Jankauskas on 11/08/2020.
//  Copyright © 2020 Palringo. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    public static func show(withSuperView parent: UIView) -> LoadingView {
        let loadingView = Bundle.main.loadNibNamed("LoadingView", owner: nil)?.first as! LoadingView
        parent.addSubview(loadingView)

        loadingView.layer.cornerRadius = 5
        loadingView.center = CGPoint(x: parent.bounds.midX,
                                     y: parent.bounds.midY)
        return loadingView
    }

}
