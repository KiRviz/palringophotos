//
//  CommentCell.swift
//  PalringoPhotos
//
//  Created by Darius Jankauskas on 11/08/2020.
//  Copyright © 2020 Palringo. All rights reserved.
//

import UIKit

class CommentCell: UICollectionViewCell {
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    public var comment: PhotoComment? {
        didSet {
            self.layer.cornerRadius = 10

            nameLabel.text = comment?.author
            commentLabel.attributedText = comment?.comment.htmlToAttributedString
        }
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        
        let targetSize = CGSize(width: layoutAttributes.frame.width, height: 0)
        
        layoutAttributes.frame.size = contentView.systemLayoutSizeFitting(
            targetSize,
            withHorizontalFittingPriority: .required,
            verticalFittingPriority: .fittingSizeLevel)
        return layoutAttributes
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        commentLabel.text = " "
        nameLabel.text = " "
    }
}
