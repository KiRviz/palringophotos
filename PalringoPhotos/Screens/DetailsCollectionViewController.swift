//
//  DetailsViewController.swift
//  PalringoPhotos
//
//  Created by Darius Jankauskas on 10/08/2020.
//  Copyright © 2020 Palringo. All rights reserved.
//

import UIKit

class DetailsCollectionViewController: UICollectionViewController {
    @IBOutlet weak var dataSource: DetailsDataSource!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    public var photo: Photo? {
        didSet {
            _ = view // load the view
            
            self.navigationItem.title = photo?.name
            
            dataSource.photo = photo
        }
    }
    
    public var image: UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
}


extension DetailsCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let screenWidth = collectionView.bounds.width

        switch DetailsSection(rawValue: indexPath.section) {
        case .comments, .none:
            return CGSize(width: screenWidth - 20, height: 200)
        case .photo:
            guard let image = image else {
                return CGSize(width: screenWidth, height: 200)
            }

            let scaledHeight = image.heightAfterShrinkingTo(width: screenWidth)
            
            return CGSize(width: screenWidth,
                          height: scaledHeight)
        }


    }
}
