//
//  DetailsCollectionViewDataSource.swift
//  PalringoPhotos
//
//  Created by Darius Jankauskas on 11/08/2020.
//  Copyright © 2020 Palringo. All rights reserved.
//

import UIKit

enum DetailsSection: Int, CaseIterable {
    case photo
    case comments
    
    var reuseIdentifier: String {
        switch self {
        case .photo:
            return "PhotoCell"
        case .comments:
            return "CommentCell"
        }
    }
}

class DetailsDataSource: NSObject, UICollectionViewDataSource {
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var comments: [PhotoComment] = []
    
    public var photo: Photo? {
        didSet {
            guard let photo = photo else { return }

            collectionView.reloadData()
            
            let loadingView = LoadingView.show(withSuperView: collectionView)

            FlickrFetcher().getPhotoComments(for: photo) { [weak self, collectionView] comments in
                self?.comments = comments
                
                // when the comments are cached, they get fetched too soon for inserting
                if self?.collectionView(collectionView!, numberOfItemsInSection: 1) == 0 {
                    let inserts = comments.map { _ in IndexPath(row: 0, section: 1) }
                    self?.collectionView?.insertItems(at: inserts)
                } else {
                    collectionView?.reloadData()
                }
                
                loadingView.removeFromSuperview()
            }
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return DetailsSection.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch DetailsSection(rawValue: section) {
        case .photo:
            return 1
        case .comments:
            return comments.count
        case .none:
            assertionFailure("no such section")
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let section = DetailsSection(rawValue: indexPath.section) else {
            return UICollectionViewCell()
        }
        
        switch section {
        case .photo:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: section.reuseIdentifier, for: indexPath) as! DetailsPhotoCell
            cell.photo = photo
            return cell
        case .comments:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: section.reuseIdentifier, for: indexPath) as! CommentCell
            cell.comment = comments[indexPath.row]
            return cell
        }
    }
}
