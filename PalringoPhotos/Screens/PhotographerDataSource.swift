//
//  AuthorDataSource.swift
//  PalringoPhotos
//
//  Created by Darius Jankauskas on 10/08/2020.
//  Copyright © 2020 Palringo. All rights reserved.
//

import UIKit

private let reuseIdentifier = "PhotographerCell"

class PhotographerDataSource: NSObject, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Photographer.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)

        cell.textLabel?.text = Photographer.allCases[indexPath.row].displayName
        
        return cell
    }
    

}
