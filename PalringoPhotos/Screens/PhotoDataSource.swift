//
//  ImageDataSource.swift
//  PalringoPhotos
//
//  Created by Benjamin Briggs on 14/10/2016.
//  Copyright © 2016 Palringo. All rights reserved.
//

import UIKit

private let reuseIdentifier = "PhotoCell"

class PhotoDataSource: NSObject, UICollectionViewDataSource {

    private var photos: [[Photo]] = []
    private var isFetchingPhotos = false
    
    public var photographer: Photographer = .dersascha {
        didSet {
            photos = []
            fetchNextPage()
            collectionView?.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
            collectionView?.reloadData()
        }
    }

    @IBOutlet private weak var collectionView: UICollectionView?

    override init() {
        super.init()
        fetchNextPage()
    }
    
    func photo(forIndexPath indexPath: IndexPath) -> Photo {
        if indexPath.section == photos.count - 1 { fetchNextPage() }
        return self.photos[indexPath.section][indexPath.item]
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return photos.count
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos[section].count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PhotoCell

        let photo = self.photo(forIndexPath: indexPath)
        cell.photo = photo

        return cell
    }
    
    private func fetchNextPage() {
        if isFetchingPhotos { return }
        isFetchingPhotos = true
        
        let loadingView = collectionView.map { LoadingView.show(withSuperView: $0) }
        
        let currentPage = photos.count
        FlickrFetcher().getPhotosUrls(photographer: photographer,
                                      page: currentPage + 1) { [weak self] photo in
            if photo.count > 0 {
                self?.photos.append(photo)
                self?.collectionView?.insertSections(IndexSet(integer: currentPage))
                self?.isFetchingPhotos = false
            }
        
            loadingView?.removeFromSuperview()
        }
    }
}
