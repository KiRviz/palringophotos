//
//  AuthorTableViewController.swift
//  PalringoPhotos
//
//  Created by Darius Jankauskas on 10/08/2020.
//  Copyright © 2020 Palringo. All rights reserved.
//

import UIKit

class PhotographerTableViewController: UITableViewController {
    
    public var onSelect: ((Photographer) -> ())?
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onSelect?(Photographer.allCases[indexPath.row])
        self.dismiss(animated: true)
    }
}
