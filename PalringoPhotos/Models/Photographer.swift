//
//  Photographers.swift
//  PalringoPhotos
//
//  Created by Darius Jankauskas on 10/08/2020.
//  Copyright © 2020 Palringo. All rights reserved.
//

import Foundation

enum Photographer: String, CaseIterable {
    case dersascha
    case alfredoliverani
    case photographybytosh
    
    var userId: String {
        return rawValue
    }

    var displayName: String {
        switch self {
        case .dersascha:
            return "Sascha Gebhardt"
        case .alfredoliverani:
            return "Alfredo Liverani"
        case .photographybytosh:
            return "Martin Tosh"
        }
    }

    var imageURL: URL {
        switch self {
        case .dersascha:
            return URL(string: "https://farm6.staticflickr.com/5489/buddyicons/26383637@N06_r.jpg")!
        case .alfredoliverani:
            return URL(string: "https://farm4.staticflickr.com/3796/buddyicons/41569704@N00_l.jpg")!
        case .photographybytosh:
            return URL(string: "https://farm9.staticflickr.com/8756/buddyicons/125551752@N05_r.jpg")!
        }
    }
}
